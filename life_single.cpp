//
// Created by Zheng Yan on 10/4/15.
//

#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <cstdlib>
#include <cstring>

using namespace std;

int xLimit = 0;
int yLimit = 0;
short directions[8][2] = {{0,  1},
                          {1,  1},
                          {1,  0},
                          {1,  -1},
                          {0,  -1},
                          {-1, -1},
                          {-1, 0},
                          {-1, 1}};

void print_map(bool *map) {
    for (int x = 0; x < xLimit; ++x) {
        for (int y = 0; y < yLimit; ++y) {
            if (map[y * xLimit + x]) {
                cout << x << " " << y << endl;
            }
        }
    }
}

int main(int argc, char *argv[]) {
    if (argc != 5) {
        cout << "./life <input file> <# of generations> <x limit> <y limit>" << endl;
        exit(-1);
    }

    string inputFile = argv[1];
    int numGeneration = atoi(argv[2]);
    xLimit = atoi(argv[3]);
    yLimit = atoi(argv[4]);

    int mapSize = xLimit * yLimit;
    bool *map = new bool[mapSize];
    memset(map, false, sizeof(bool) * mapSize);

    ifstream fin(inputFile.c_str());
    int a, b;
    while (fin >> a >> b) {
        map[b * xLimit + a] = true;
    }

    short *newMap = new short[mapSize];
    for (int gene = 1; gene <= numGeneration; ++gene) {
        memset(newMap, 0, sizeof(short) * mapSize);
        for (int x = 0; x < xLimit; ++x) {
            for (int y = 0; y < yLimit; ++y) {
                int pos = y * xLimit + x;
                for (int t = 0; t < 8; ++t) {
                    if (0 <= x + directions[t][0] && x + directions[t][0] < xLimit && 0 <= y + directions[t][1] &&
                        y + directions[t][1] < yLimit) {
                        if (map[(y + directions[t][1]) * xLimit + x + directions[t][0]]) {
                            newMap[pos] += 1;
                        }
                    }
                }
            }
        }
        for (int x = 0; x < xLimit; ++x) {
            for (int y = 0; y < yLimit; ++y) {
                int pos = y * xLimit + x;
                if (!map[pos]) {
                    if (newMap[pos] == 3) {
                        map[pos] = true;
                    }
                } else {
                    if (newMap[pos] < 2 || newMap[pos] > 3) {
                        map[pos] = false;
                    }
                }
            }
        }
    }
    print_map(map);

    delete map;
    return 0;
}