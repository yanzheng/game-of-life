#include <iostream>
#include <fstream>
#include <cstdlib>
#include <mpi.h>

using namespace std;

typedef char dtype;
int xLimit = 0;
int yLimit = 0;
dtype directions[8][2] = {{0,  1},
                          {1,  0},
                          {1,  1},
                          {1,  -1},
                          {0,  -1},
                          {-1, 0},
                          {-1, -1},
                          {-1, 1}};
dtype *gameMap;

void print_map(dtype *map) {
    for (int x = 0; x < xLimit; ++x) {
        for (int y = 0; y < yLimit; ++y) {
            if (map[y * xLimit + x] > 0) {
                cout << x << " " << y << endl;
            }
        }
    }
}

void read_map(string inputFile) {
    ifstream fin(inputFile.c_str());
    int a, b;
    while (fin >> a >> b) {
        gameMap[b * xLimit + a] = 1;
    }
}

int main(int argc, char *argv[]) {
    if (argc != 5) {
        cout << "./life <input file> <# of generations> <x limit> <y limit>" << endl;
        return -1;
    }

    string inputFile = argv[1];
    int numGeneration = atoi(argv[2]);
    xLimit = atoi(argv[3]);
    yLimit = atoi(argv[4]);

    int myRank, mpiSize;
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);

    int mapSize = xLimit * (yLimit / mpiSize);

    if (myRank == 0) {
        // Keep remainder cells
        int mySize = mapSize + (xLimit * (yLimit % mpiSize));

        gameMap = new dtype[xLimit * yLimit + xLimit];
        memset(gameMap, 0, sizeof(dtype) * (xLimit * yLimit + xLimit));
        read_map(inputFile);

        for (int i = 1; i < mpiSize; ++i) {
            MPI_Send(gameMap + mySize + mapSize * (i - 1) - xLimit, mapSize + xLimit * 2, MPI_CHAR, i, 99,
                     MPI_COMM_WORLD);
        }

        dtype *conv = new dtype[mySize + xLimit];
        for (int gene = 1; gene <= numGeneration; ++gene) {
            memset(conv, 0, sizeof(dtype) * (mySize + xLimit));

            for (int x = 0; x < xLimit; ++x) {
                for (int y = 0; y < yLimit / mpiSize + yLimit % mpiSize; ++y) {
                    for (int t = 0; t < 8; ++t) {
                        if (0 <= x + directions[t][0] && x + directions[t][0] < xLimit && 0 <= y + directions[t][1] &&
                            y + directions[t][1] < yLimit) {
                            if (gameMap[(y + directions[t][1]) * xLimit + x + directions[t][0]] > 0) {
                                conv[y * xLimit + x] += 1;
                            }
                        }
                    }
                }
            }

            for (int x = 0; x < xLimit; ++x) {
                for (int y = 0; y < yLimit / mpiSize + yLimit % mpiSize; ++y) {
                    if (gameMap[y * xLimit + x] == 0) {
                        if (conv[y * xLimit + x] == 3) {
                            gameMap[y * xLimit + x] = 1;
                        }
                    } else {
                        if (!(conv[y * xLimit + x] == 2 || conv[y * xLimit + x] == 3)) {
                            gameMap[y * xLimit + x] = 0;
                        }
                    }
                }
            }

            if (mpiSize > 1) {
                MPI_Send(gameMap + mySize - xLimit, xLimit, MPI_CHAR, 1, 99, MPI_COMM_WORLD);
                MPI_Recv(gameMap + mySize, xLimit, MPI_CHAR, 1, 99, MPI_COMM_WORLD, &status);
            }
        }

        // TODO: MPI_Gather
        dtype *newMap = new dtype[xLimit * yLimit + xLimit];
        memcpy(newMap, gameMap, mySize * sizeof(dtype));
        for (int i = 1; i < mpiSize; ++i) {
            MPI_Recv(newMap + mySize + mapSize * (i - 1), mapSize, MPI_CHAR, i, 99, MPI_COMM_WORLD, &status);
        }
        print_map(newMap);
    } else {
        gameMap = new dtype[mapSize + xLimit * 2];
        MPI_Recv(gameMap, mapSize + xLimit * 2, MPI_CHAR, 0, 99, MPI_COMM_WORLD, &status);

        dtype *conv = new dtype[mapSize + xLimit * 2];
        for (int gene = 1; gene <= numGeneration; ++gene) {
            memset(conv, 0, sizeof(dtype) * (mapSize + xLimit * 2));

            for (int x = 0; x < xLimit; ++x) {
                for (int y = 1; y <= yLimit / mpiSize; ++y) {
                    for (int t = 0; t < 8; ++t) {
                        if (0 <= x + directions[t][0] && x + directions[t][0] < xLimit && 0 <= y + directions[t][1] &&
                            y + directions[t][1] < yLimit) {
                            if (gameMap[(y + directions[t][1]) * xLimit + x + directions[t][0]] > 0) {
                                conv[y * xLimit + x] += 1;
                            }
                        }
                    }
                }
            }

            for (int x = 0; x < xLimit; ++x) {
                for (int y = 1; y <= yLimit / mpiSize; ++y) {
                    if (gameMap[y * xLimit + x] == 0) {
                        if (conv[y * xLimit + x] == 3) {
                            gameMap[y * xLimit + x] = 1;
                        }
                    } else {
                        if (!(conv[y * xLimit + x] == 2 || conv[y * xLimit + x] == 3)) {
                            gameMap[y * xLimit + x] = 0;
                        }
                    }
                }
            }

            MPI_Send(gameMap + xLimit, xLimit, MPI_CHAR, myRank - 1, 99, MPI_COMM_WORLD);
            if (myRank != mpiSize - 1)
                MPI_Send(gameMap + mapSize, xLimit, MPI_CHAR, myRank + 1, 99, MPI_COMM_WORLD);

            MPI_Recv(gameMap, xLimit, MPI_CHAR, myRank - 1, 99, MPI_COMM_WORLD, &status);
            if (myRank != mpiSize - 1)
                MPI_Recv(gameMap + xLimit + mapSize, xLimit, MPI_CHAR, myRank + 1, 99, MPI_COMM_WORLD, &status);
        }

        MPI_Send(gameMap + xLimit, mapSize, MPI_CHAR, 0, 99, MPI_COMM_WORLD);
    }

    MPI_Finalize();
    return 0;
}
